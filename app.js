$ ->
    $("ul.board").sortable
        connectWith: "ul.board"
        receive: ->
            console.log "received"

        remove: ->
            console.log "removed"



class Kanban extends Spine.Controller
    ENTER_KEY = 13

    constructor: (@el) ->
        super
        
        Card.fetch()

        #a = new Card text: "Test #2", done: false
        #a.save()

        col = Column.create()
        console.log col.cards()

        cards = Card.all()

        this.render cards

    render: (@cards) ->
        for card in cards
            #console.log card
            console.log card.text + " -> " + (if card.done == true then "done" else "undone") 


class Card extends Spine.Model
    @configure "Card", "text", "done"
    #@extend Spine.Model.Ajax
    @extend Spine.Model.Local
    @belongsTo 'column', 'Column'

class Column extends Spine.Model
    @configure "Card", "title"
    @extend Spine.Model.Local
    @hasMany 'cards', 'Card'


$ ->
    new Kanban el: $('ul.board')